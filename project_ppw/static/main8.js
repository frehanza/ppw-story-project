const renderBook = ({
    title,
    authors,
    coverImgSrc,
    isHead
  }={}) => {
    const row = document.createElement('tr')
    row.className = 'row'
  
    let img = row.insertCell(0)
    img.innerHTML = isHead ? `<h1>Cover</h1>` : `<img src=${coverImgSrc}></img>`
    img.className = 'img-col'
  
    let titleElement = row.insertCell(1)
    titleElement.innerHTML = `<h1>${isHead ? 'Title' : title}</h1>`
    titleElement.className = 'title-col'
  
    let authorsElement = row.insertCell(2)
    authorsElement.innerHTML = isHead
      ? '<h1>Author(s)</h1>'
      : `<ul>
          ${authors && authors.length > 0
            ?
            authors.map((name, idx) => {
              return `<li>${name}</li>`
            })
            :
            'No Author(s)'
            }
        </ul>`
    authorsElement.className = 'authors-col'
  
    // Add item to main div
    document.querySelector('.search-results').appendChild(row)
  }
  
  const setSearchResultCount = num => {
    document.querySelector('#search-result-count').innerHTML = `${num} books found`
  }
  
  const clearSearchResults = () => {
    document.querySelector('.search-results').innerHTML = ''
  }
  
  const search = async keyword => {
    fetch(`/search/${keyword}`)
      .then((res) => {
        res.json()
          .then(jsonData => {
            clearSearchResults()
            setSearchResultCount(jsonData.data.totalItems)
            const bookArr = jsonData.data.items
  
            // Initiate table
            renderBook({
              isHead: true
            })
  
            bookArr.map(book => {
              const { volumeInfo } = book
              const { title, authors } = volumeInfo
              const coverImgSrc = volumeInfo.imageLinks.smallThumbnail
              renderBook({
                title,
                authors,
                coverImgSrc
              })
            })
          })
          .catch(err => console.log(err))
      })
  }
  
  window.addEventListener('load', () => {
    // Read keyword from input
    let keyword
    document.querySelector('input').addEventListener('change', e => {
      keyword = e.target.value
    })
  
    // Default books
    search('Harry Potter')
  
    document.querySelector('#search-box').addEventListener('submit', (e) => {
      e.preventDefault()
      // Submit data
      search(keyword)
  
    })
  })