"""project_ppw URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.1/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include, re_path
from django.conf import settings
from django.conf.urls.static import static

from story_7.views import index, add_data, get_data
from story_8.views import index, search
from story_9.views import index, auth, api_login, api_sign_up, logout_user

urlpatterns = [
    path('admin/', admin.site.urls),
    path('', include('landingpage.urls')),
    
    path('story-7/', include('story_7.urls')),
    path('add-data/', add_data, name='add-data'),
    path('get-data/', get_data, name='get-data'),

    path('story-8/', include('story_8.urls')),
    path('search/<str:keyword>', search, name='search'),
    path('search/', search, name='search'),

    path('story-9/', include('story_9.urls')),
    path('', index, name="home"),
    path('auth/', auth, name="authentication_page"),
    path('logout/', logout_user, name="logout"),
    path('api/v1/login/', api_login, name="login"),
    path('api/v1/signup/', api_sign_up, name="sign_up"),

]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

