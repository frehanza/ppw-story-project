from django.urls import path, include
from .views import index,add_data,get_data

app_name = 'story_7'

urlpatterns = [
    path('', index, name='home'),
    path('add-data/', add_data, name='add-data'),
    path('get-data/', get_data, name='get-data'),
]   
    
  