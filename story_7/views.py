from django.shortcuts import render
from django.http import HttpResponseBadRequest, JsonResponse, HttpResponse
from django.shortcuts import redirect
from django.core import serializers

# models
from story_7.models import AccordionItem

def index(request):
  return render(request, 'main.html')

def get_data(request):
  serialized_items = serializers.serialize('json', AccordionItem.objects.all())
  return HttpResponse(serialized_items, content_type="text/json-comment-filtered")

def add_data(request):
  if request.method == 'POST':
    try:
      title = request.POST['title']
      content = request.POST['content']
    except KeyError:
      return HttpResponseBadRequest('Incomplete data')

    item = AccordionItem.objects.create(
      title=title,
      content=content
    )

    serialized_item = serializers.serialize('json', [item])

    return HttpResponse(serialized_item, content_type="text/json-comment-filtered")

  else:
    return HttpResponseBadRequest('Get request is not allowed')
