const data = [
    {
      title: 'Nama',
      content: 'Fareeha Nisa Zayda Azeeza'
    },
    {
      title: 'Hobi',
      content: 'Makan, Design, Menari, Jalan-jalan, Fangirling'
    },
    {
      title: 'Impian',
      content: 'Menjadi orang sukses'
    },
    {
      title: 'Interest',
      content: 'Art, Design, Music'
    },
  ]
  
  function getCookie(c_name)
  {
      if (document.cookie.length > 0)
      {
          c_start = document.cookie.indexOf(c_name + "=");
          if (c_start != -1)
          {
              c_start = c_start + c_name.length + 1;
              c_end = document.cookie.indexOf(";", c_start);
              if (c_end == -1) c_end = document.cookie.length;
              return unescape(document.cookie.substring(c_start,c_end));
          }
      }
      return "";
  }
  
  const swapAccordionElement = (item1, item2) => {
    if (item1 && item2) {
      const temp = item1
      item1.replaceWith(item2)
      item2.after(temp)
    }
  }
  
  const addItem = (title, content) => {
    // Create Item JQuery Objects
    const item = $(`
      <div class='item'>
        <div class='head'>
          ${title}
          <div class="arrows">
          </div>
        </div>
        <div class='content'>
          ${content}
        </div>
      </div>
    `)
    const downBtn = $("<div class='down'>&#x21e9;</div>")
    downBtn.click(() => swapAccordionElement(item[0], item.next()[0]))
    const upBtn = $("<div class='up'>&#x21e7;</div>")
    upBtn.click(() => swapAccordionElement(item.prev()[0], item[0]))
  
    // Add arrows
    item.find('.arrows').append(downBtn)
    item.find('.arrows').append(upBtn)
  
    // Add click event listener
    item.click((e) => {
      $(e.target).next().toggleClass('show')
    })
  
    // Add item to accordion
    $('#accordion').append(item)
  }
  
  const getDataFromBackend = () => {
    $.ajax({
      url: "/get-data/",
      method : "get",
      dataType : "json",
      success: (res) => {
        res.map((obj) => addItem(obj.fields.title, obj.fields.content))
      },
      error: () => {
        alert('Failed to fetch data')
      }
    })
  }
  
  // Main
  $('document').ready(() => {
    getDataFromBackend()
  
    // form
    const ajaxData = {}
    $('input, textarea').change(e => {
      ajaxData[e.target.name] = e.target.value
    })
  
    $('form').submit((e) => {
      e.preventDefault()
      let valid_form = true;
  
      Object.values(ajaxData).map(item => {
        if (item === null || item.length === 0) {
          valid_form = false;
        }
      })
  
      if (valid_form) {
        $.ajax({
          url: "/add-data/",
          method : "post",
          dataType : "json",
          data : ajaxData,
          headers: { "X-CSRFToken": getCookie("csrftoken") },
          success: (res) => {
            $('input, textarea').val('')
            const {title, content} = res[0].fields
            addItem(title, content)
          },
          error: (err) => {
            alert('Team creation failed')
          }
        })
      }
    })
  })