from django.test import TestCase, Client
from django.http import HttpRequest

# views
from .views import index, add_data, get_data

# models
from .models import AccordionItem

class TestUrls(TestCase):
  # Test if base url exists
  def test_base_url_exists(self):
    response = Client().get('')
    return self.assertEquals(response.status_code, 200)

  def test_base_url_template(self):
    response = Client().get('')
    return self.assertTemplateUsed(response, 'index.html')

  # Test if add-data url exists
  def test_add_data_url_exists(self):
    response  = Client().get('/add-data/')
    return self.assertEquals(response.status_code, 400)

  def test_get_data_url_exists(self):
    response = Client().get('/get-data/')
    return self.assertEquals(response.status_code, 200)

class TestViews(TestCase):
  # Test if index view works
  def test_index_response(self):
    return self.assertEquals(index(HttpRequest()).status_code, 200)

  def test_index_template(self):
    response = index(HttpRequest())
    html_response = response.content.decode('utf8')
    return self.assertIn('Accordion', html_response)

  def test_add_data_response_without_data(self):
    request = HttpRequest()
    request.method = 'POST'
    response = add_data(request)

    # Get request will return 400
    return self.assertEquals(response.status_code, 400)

  def test_get_data_response(self):
    return self.assertEquals(get_data(HttpRequest()).status_code, 200)

  def test_add_data_response_with_data(self):
    request = HttpRequest()
    request.method = 'POST'
    request.POST['title'] = 'title'
    request.POST['content'] = 'content'
    response = add_data(request)

    # Return json if post request success
    return self.assertEquals(response.status_code, 200)

class TestModels(TestCase):
  # Test if model count increases
  def test_accordion_item_object_count(self):
    AccordionItem.objects.create(
      title='title',
      content='content'
    )
    self.assertEquals(AccordionItem.objects.count(), 1)

  def test_accordion_item_str(self):
    item = AccordionItem.objects.create(
      title='title',
      content='content'
    )
    self.assertEquals(str(item), f'{item.title}: {item.content}')
