from django.contrib import admin

# models
from .models import AccordionItem

admin.site.register(AccordionItem)
