
from django.contrib import admin
from django.urls import path, include, re_path

# views
from story_8.views import index, search

app_name = 'story_8'

urlpatterns = [
    path('', index, name='home'),
    path('search/<str:keyword>', search, name='search'),
    path('search/', search, name='search'),
]