from django.shortcuts import render, redirect
from django.http import HttpResponse, JsonResponse, HttpResponseBadRequest
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.models import User
from django.views.decorators.csrf import csrf_exempt


import json

# Create your views here.
def index(request):
    if request.user.is_authenticated:
        context = {
            'user': request.user
        }
        return render(request, 'main9.html', context)
    else:
        return redirect('/auth/')

def auth(request):
    return render(request, 'login.html')

def logout_user(request):
    logout(request)
    return redirect('/auth/')

@csrf_exempt
def api_login(request):
    if request.method == 'POST':
        data = json.loads(request.body)
        if 'username' in data and 'password' in data:
            user = authenticate(request, username=data['username'], password=data['password'])

            if user is not None:
                login(request, user)
                return JsonResponse({
                    'status': 200,
                    'message': 'Sukses melakukan login'
                })
            else :
                try:
                    User.objects.get(username=data['username'])
                    return JsonResponse({
                        'status' : 401,
                        'message': 'Username / password salah, mohon periksa kembali dan coba lagi'
                    })
                except User.DoesNotExist:
                    return JsonResponse({
                        'status' : 401,
                        'message': 'Username tidak terdaftar, mohon periksa kembali dan coba lagi'
                    })

    return HttpResponseBadRequest()

@csrf_exempt
def api_sign_up(request):
    if request.method == 'POST':
        data = json.loads(request.body)

        if 'username' in data and 'password' in data and 'email' in data:
            try:
                User.objects.create_user(data['username'], data['email'], data['password'])
                return JsonResponse({
                    'status': 200,
                    'message': 'Sukses membuat akun, silahkan login dengan akun yang anda buat!'
                })
            except:
                return JsonResponse({
                    'status' : 500,
                    'message': 'Username sudah dipakai, coba ganti dengan username lain'
                })
                
    return HttpResponseBadRequest()