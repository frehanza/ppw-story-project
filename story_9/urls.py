from django.contrib import admin
from django.urls import path, include

from story_9.views import index, auth, logout_user, api_login, api_sign_up

app_name = 'story_9'

urlpatterns = [
    path('', index, name="homepage"),
    path('auth/', auth, name="authentication_page"),
    path('logout/', logout_user, name="logout"),
    path('api/v1/login/', api_login, name="login"),
    path('api/v1/signup/', api_sign_up, name="sign_up"),
]