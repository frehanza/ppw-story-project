from django.test import TestCase , Client, LiveServerTestCase
from django.http import HttpRequest
from django.contrib.auth.models import User
from django.contrib.auth import authenticate, login
from selenium import webdriver
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.support.ui import WebDriverWait

import os
import time

class SmokeTest(TestCase):
  def test_does_pipeline_work(self):
    return self.assertEquals(1, 1)

class TestUrls(TestCase):
  def test_index_url_exists(self):
    response = Client().get('')
    self.assertEquals(response.status_code, 200)

class LoginpageUnitTest(TestCase):
    def test_redirect_to_login_page(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)
    
    def test_bad_request_api(self):
        response = Client().get('/api/v1/login/')
        self.assertEqual(response.status_code, 400)

        response = Client().get('/api/v1/signup/')
        self.assertEqual(response.status_code, 400)

