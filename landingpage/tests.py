from django.test import TestCase , Client
from django.http import HttpRequest
from django.apps import apps
from django.urls import resolve

# views
from . import views


class GeneralTest(TestCase):
  def test_does_pipeline_work(self):
    self.assertEquals(1, 1)

  def test_status(self):
    response = Client().get('')
    self.assertEqual(response.status_code, 200)

class TestURLS(TestCase):
  # Testing urls
  def test_base_url_exists(self):
    response = Client().get('')
    self.assertEquals(response.status_code, 200)

  def test_base_url_template(self):
    response = self.client.get('')
    self.assertTemplateUsed(response, 'index.html')

class TestViews(TestCase):
  # Testing views
  def test_index_view_response(self):
    response = views.index(HttpRequest())
    self.assertEquals(response.status_code, 200)

  def test_using_func(self):
      found = resolve('/')
      self.assertEqual(found.func, views.index)

  
  
  





