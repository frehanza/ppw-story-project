from django.shortcuts import render
from django.http import HttpResponseBadRequest, JsonResponse, HttpResponse
from django.shortcuts import redirect
from django.core import serializers

def index(request):
  return render(request, 'index.html')
